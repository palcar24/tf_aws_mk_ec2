# tf_aws_mk_ec2

Creando una instancia EC2 en AWS con TerraForm

## Descripción

![Diagrama](https://shorturl.at/qvFSY)


Este plantilla de terraform se realizo por Carlos Palacios el 05/10/21 a proposito del reto propuesto por [Nivelics](http://www.nivelics.com/), y pretende crear una instancia EC2.

## Pre-Requisitos

To follow this tutorial you will need:

- [ ] El CLI de Terraform (0.14.9+) instalado.
- [ ] El CLI AWS instalado.
- [ ] Una Cuenta AWS.

**Nota:** Es necesario tengas configurada tu Access key con el comando ***aws configure***

## Como Desplegar desde Linux?

```
git clone https://gitlab.com/palcar24/tf_aws_mk_ec2.git
cd tf_aws_mk_ec2
terraform apply
```

Luego terraform hara un resumen y

```
.
.
.
Plan: 1 to add, 0 to change, 0 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes
```

Si todo va bien:

```
aws_instance.app_server: Creating...
aws_instance.app_server: Still creating... [10s elapsed]
aws_instance.app_server: Still creating... [20s elapsed]
aws_instance.app_server: Creation complete after 21s [id=i-0ca7ce8684f24e6c8]

Apply complete! Resources: 1 added, 0 changed, 0 destroyed.
```
